// FB initiation function
window.fbAsyncInit = () => {
  FB.init({
    appId      : '511770225860848',
    cookie     : true,
    xfbml      : true,
    version    : 'v2.11'
  });

  // implementasilah sebuah fungsi yang melakukan cek status login (getLoginStatus)
  // dan jalankanlah fungsi render di bawah, dengan parameter true jika
  // status login terkoneksi (connected)
  FB.getLoginStatus(function(response) {
    console.log(response.status);
        if (response.status === 'connected') {
          render(true);
          console.log("connected yey");
        }
        else{
          render(false);
          console.log("yah");
        }
     });

  // Hal ini dilakukan agar ketika web dibuka dan ternyata sudah login, maka secara
  // otomatis akan ditampilkan view sudah login
};

// Call init facebook. default dari facebook
(function(d, s, id){
   var js, fjs = d.getElementsByTagName(s)[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement(s); js.id = id;
   js.src = "https://connect.facebook.net/en_US/sdk.js";
   fjs.parentNode.insertBefore(js, fjs);
 }(document, 'script', 'facebook-jssdk'));

// Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
// merender atau membuat tampilan html untuk yang sudah login atau belum
// Ubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
// Class-Class Bootstrap atau CSS yang anda implementasi sendiri
const render = loginFlag => {
  if (loginFlag) {
    // Jika yang akan dirender adalah tampilan sudah login

    // Memanggil method getUserData (lihat ke bawah) yang Anda implementasi dengan fungsi callback
    // yang menerima object user sebagai parameter.
    // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
    getUserData(user => {
      // Render tampilan profil, form input post, tombol post status, dan tombol logout
      console.log(user);
      console.log("asyik");
      $("#lab8").html(
        '<div class="profile">' +
          '<img class="cover" src="' + user.cover.source + '" alt="cover" />' +
          '<img class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
          '<div class="data">' +
            '<h1>' + user.name + '</h1>' +
            '<p>' + user.about + '</p>' +
            '<p>' + user.email + ' - ' + user.gender + '</p>' +
          '</div>' +
        '</div>' +
        '<div id="inputs">'+
        '<input id="postInput" type="text" class="post" placeholder="Ketik Status Anda" />' +
        '</div>'+
        '<div id="inputButtons">'+
        '<button class="postStatus" onclick="postStatus()">Post</button>' +
        '</div>'
      );

      $("#navbar").html('<a class="logout" onclick="facebookLogout()">Logout</a>')

      // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
      // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
      // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
      // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
      getUserFeed(feed => {
        feed.data.map(value => {
          // Render feed, kustomisasi sesuai kebutuhan.
          $("#lab8").append(
            '<div id="feeds"></div>'
          )
          console.log(value);
          if (value.message && value.story) {
            $("#feeds").append(
              '<div class="feed message"> id="post_'+ value.id +'">' +
                '<p>' + value.message + '</p>' +
                '<button class="delete" onclick="deletePost(\'' + value.id + '\')">delete</button>' +
              '</div>'+
              '<div class="feed story">' +
                '<p>' + value.story + '</p>' +
              '</div>'
            );
          } else if (value.message) {
            $("#feeds").append(
              '<div class="feed message" id="post_'+ value.id +'">' +
                '<p>' + value.message + '</p>' +
                '<button class="delete" onclick="deletePost(\'' + value.id + '\')">delete</button>' +
              '</div>'
            );
          } else if (value.story) {
            $("#feeds").append(
              '<div class="feed story">' +
                '<p>' + value.story + '</p>' +
              '</div>'
            );
          }
        });
      });
    });
  } else {
    // Tampilan ketika belum login
    $("#navbar").html('<a class="login" onclick="facebookLogin()">Login with Facebook</a>');
    $("#lab8").html('<h1 id="loginMenu">Kuy Login</h1>');

  }
};

const facebookLogin = () => {
  // TODO: Implement Method Ini
  // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan sudah login
  // ketika login sukses, serta juga fungsi ini memiliki segala permission yang dibutuhkan
  // pada scope yang ada. Anda dapat memodifikasi fungsi facebookLogin di atas.
  FB.login(function(response){
    console.log(response);
    render(true);
  }, {scope:'public_profile,user_posts,publish_actions,email,user_about_me'})
};

const facebookLogout = () => {
  // TODO: Implement Method Ini
  // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
  // ketika logout sukses. Anda dapat memodifikasi fungsi facebookLogout di atas.
  FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
          console.log("otw logout");
          FB.logout();
          render(false);
        }
     });

};

// TODO: Lengkapi Method Ini
// Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
// lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di
// method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan
// meneruskan response yang didapat ke fungsi callback tersebut
// Apakah yang dimaksud dengan fungsi callback?
const getUserData = (fun) => {
  FB.getLoginStatus(function(response) {
     if (response.status === 'connected') {
       FB.api('/me?fields=id,name,cover,picture,about,email,gender', 'GET', function(response){
         console.log(response);
         fun(response);
       });
     }
 });
};

const getUserFeed = (fun) => {
  // TODO: Implement Method Ini
  // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
  // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
  // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
  // tersebut
  FB.getLoginStatus(function(response){
    if (response.status === 'connected'){
      FB.api('/me/feed', 'GET', function(response){
        console.log(response);
        fun(response);
      });
    }
  })

};

const postFeed = (message) => {
  // Todo: Implement method ini,
  // Pastikan method ini menerima parameter berupa string message dan melakukan Request POST ke Feed
  // Melalui API Facebook dengan message yang diterima dari parameter.
    FB.api('/me/feed', 'POST', {message:message});
    render(true);
};

const postStatus = () => {
  const message = $("#postInput").val();
  console.log(message);
  postFeed(message);
};

const deletePost = (postId) => {
  FB.api(
    postId,
    "DELETE",
    function (response) {
      if (response && !response.error) {
        /* handle the result */
        $("#post_" + postId).remove();
      }
    }
);
}
